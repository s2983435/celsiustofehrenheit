package nl.utwente.di.bookQuote;

import java.util.*;
import java.util.Map;

public class Quoter {
    public double getFTemp(String stCelsius) {
        double celsius = Double.parseDouble(stCelsius);
        return (celsius*1.8)+32;
    }
}
