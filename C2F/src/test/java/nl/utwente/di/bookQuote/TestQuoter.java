package nl.utwente.di.bookQuote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
        /** * Tests the Quoter */
public class TestQuoter {

    @Test
    public void testTemp1() throws Exception {
        Quoter quoter = new Quoter ();
        double price = quoter.getFTemp("1");
        Assertions.assertEquals(10.0, price, 0.0, "Celsius to Fahrenheit") ;
    }
}